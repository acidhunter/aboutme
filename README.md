Install
-------
Prerequisites
  - virtualenv
  - python 2.7
  - build-essential


Create a virtual environment:
```bash
virtualenv -p /usr/bin/python2.7 env
```
Activate virtual environment
```bash
source env/bin/activate
```
Install modules with pip
```bash
pip install -r requirements.txt
```

Start mkdocs
```bash
mkdocs serve
```
