# Skills
- Linux (Debian based, Arch Linux, CentOS)
- Windows (all)
- macOS
- FreeBSD

### Storage
- ZFS, LVM, Btrfs
- Network Attached Storage
- Storage-Area-Network

### Network
- Firewall (pfSense, IPtables, ebtables)
- Intrusion Detection & Prevention (Suricata)
- VLAN
- VPN (openVPN, IPsec)
- WiFi-Hotspots (dd-wrt, open-wrt, lede, [Freifunk](https://map.ffmuc.net/#!v:m;n:c46e1f93213c))

### Virtualisation
- ProxmoxVE
- KVM
- LXC
- Docker
- libvirt

### Monitoring
- Check_MK (Nagios / Icinga)
- Amon
- Telegraf / Grafana [demo](https://grafana.kmpt.nz/dashboard/snapshot/KTIPSBuBONy2SLPWQcO2P4xZKQ4II6Mm?orgId=1)

### Web-Apps
- Nginx
- HAProxy

### Databases
- MySQL / MariaDB
- PostgreSQL
- InfluxDB

### Domain Administration
- Samba / AD / OpenLDAP / [Univention Corporate Server](https://www.univention.com/)
- Windows, Linux, macOS clients via (LDAP / AD)
- Printserver
- PKI Logon
- Client provisioning via PXE (Debian based)

### Mailserver
- Dovecot, Postfix
- Amavis, ClamAV
- Zimbra
- Kopano / Zarafa

### Other Software
- EjabberD
- Mobydick PBX
