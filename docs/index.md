

Geboren                München, 11.09.1988

Staatsangehörigkeit    Deutsch

## Sprachen
 - Deutsch
 - Englisch

## Hobbies
 - Computer generated Art
 - Ray-tracing & Network Rendering
 - Community Building
 - GL-Shader

## Kontakt
 - [Mail](mailto:npanic@kmpt.nz)
 - [Github](https://github.com/acidhunter)
 - [PGP key](https://keybase.io/npanic/pgp_keys.asc)
 - [btc wallet](bitcoin:133MYAoMHwqtiiYXGYGPkNq8iv1qDNcLRW)

## Projects
### [dotfiles](https://git.kmpt.nz/npanic/dotfiles)
### [Mastodon](https://joinmastodon.org/) instance at [Acid.wtf](https://acid.wtf)
### [ProxmoxVE LXC template buildbot (Gitlab-CI)](https://git.kmpt.nz/npanic/dab-ubuntu-minimal)

### Nextcloud (Private)
- LDAP based login, complex Group Mapping
- WebRTC Meetings, STUN/TURN Setup
- Onlyoffice, Etherpad and draw.io Integration

### [Dockercask (fork)](https://git.kmpt.nz/npanic/dockercask)
A script to run Desktop Applications securely inside Docker containers

### [mandelbulber2-headless](https://github.com/acidhunter/mandelbulber2-headless)
An Arch Linux based Docker image to launch rendernodes for mandelbulber2 [Dockerhub](https://hub.docker.com/r/acidhunter/mandelbulber2-headless)


[![pipelinestatus](https://git.kmpt.nz/npanic/aboutme/badges/master/pipeline.svg)](https://git.kmpt.nz/npanic/aboutme/commits/master)
