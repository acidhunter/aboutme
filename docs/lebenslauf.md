
## Berufliche Erfahrung

### 2015 - Heute
#### Systemadministrator - [Hörer & Flamme GmbH](https://hoerer-flamme.com)

 - Domänenadministration mittels Samba und OpenLDAP
 - Entwicklung von CI Workflows mit Gitlab
 - Administration von Webapplikationen
 - Datenschutz Compliance
 - Management von Marketing-Kampagnen
 - Betreuung eines Auszubildenden

### 2014 - 2015
#### Selbstständige Tätigkeit

 - Vertrieb mobiler Notrufgeräte & GPS-Trackern
 - Schulung von Leitstellen-Personal
 - Anbindung von Fernmeldeanlagen an Leitstellen-Software (AMWin)

### 2013 - 2014
#### Leitstellendisponent - SOS Service GmbH

 - Bearbeitung kritischer Alarme & Notrufe
 - Schichtleitung einer Notrufleitstelle
 - Pflege von Alarmplänen und Kundendaten
 - Telefon Support
